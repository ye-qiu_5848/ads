// config/index.js
require('env2')('./config/.env'); //设置环境变量

module.exports = {
  host:        process.env.HOST,
  port:        process.env.PORT,
  db_dialect:  process.env.DB_DIALECT,
  db_host:     process.env.DB_HOST, 
  db_port:     process.env.DB_PORT,        
  db_name:     process.env.DB_NAME,   
  db_username: process.env.DB_USERNAME,  
  db_password: process.env.DB_PASSWORD,
  path_upload_adfile: process.env.PATH_UPLOAD_ADFILE
}

