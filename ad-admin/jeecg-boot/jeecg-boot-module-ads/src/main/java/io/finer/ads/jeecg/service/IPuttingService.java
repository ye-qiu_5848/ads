package io.finer.ads.jeecg.service;

import io.finer.ads.jeecg.entity.Putting;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 广告投放
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
public interface IPuttingService extends IService<Putting> {

}
