package io.finer.ads.jeecg.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 广告投放
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Data
@TableName("ads_put")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ads_put对象", description="广告投放")
public class Putting implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "ID")
    private java.lang.String id;
	/**广告位*/
	@Excel(name = "广告位", width = 15, dictTable = "ads_slot", dicText = "name", dicCode = "id")
	@Dict(dictTable = "ads_slot", dicText = "name", dicCode = "id")
    @ApiModelProperty(value = "广告位")
    private java.lang.String slotId;
	/**广告*/
	@Excel(name = "广告", width = 15, dictTable = "ads_ad", dicText = "name", dicCode = "id")
	@Dict(dictTable = "ads_ad", dicText = "name", dicCode = "id")
    @ApiModelProperty(value = "广告")
    private java.lang.String adId;
	/**投放类型*/
	@Excel(name = "投放类型", width = 15, dicCode = "ads_put_type")
	@Dict(dicCode = "ads_put_type")
    @ApiModelProperty(value = "投放类型")
    private java.lang.Integer putType;
	/**开始时间*/
	@Excel(name = "开始时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "开始时间")
    private java.util.Date startTime;
	/**结束时间*/
	@Excel(name = "结束时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "结束时间")
    private java.util.Date endTime;
	/**展示数不超过*/
	@Excel(name = "展示数不超过", width = 15)
    @ApiModelProperty(value = "展示数不超过")
    private java.lang.Integer showMax;
	/**点击数不超过*/
	@Excel(name = "点击数不超过", width = 15)
    @ApiModelProperty(value = "点击数不超过")
    private java.lang.Integer clickMax;
	/**是否统计点击量*/
	@Excel(name = "是否统计点击量", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否统计点击量")
    private java.lang.Integer clickCountable;
	/**投放权重*/
	@Excel(name = "投放权重", width = 15)
    @ApiModelProperty(value = "投放权重")
    private java.lang.Integer weight;
	/**每人每天投放次数*/
	@Excel(name = "每人每天投放次数", width = 15)
    @ApiModelProperty(value = "每人每天投放次数")
    private java.math.BigDecimal dayTimes;
	/**启用*/
	@Excel(name = "启用", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "启用")
    private java.lang.Integer enabled;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remark;
	/**创建者*/
    @ApiModelProperty(value = "创建者")
    private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**修改者*/
    @ApiModelProperty(value = "修改者")
    private java.lang.String updateBy;
	/**修改时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date updateTime;
	/**展示数*/
	@Excel(name = "展示数", width = 15)
    @ApiModelProperty(value = "展示数")
    private java.lang.Integer showCount;
	/**点击数*/
	@Excel(name = "点击数", width = 15)
    @ApiModelProperty(value = "点击数")
    private java.lang.Integer clickCount;
	/**当天日期*/
	@Excel(name = "当天日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "当天日期")
    private java.util.Date currDate;
	/**当天展示数*/
	@Excel(name = "当天展示数", width = 15)
    @ApiModelProperty(value = "当天展示数")
    private java.lang.Integer currShowCount;
}
