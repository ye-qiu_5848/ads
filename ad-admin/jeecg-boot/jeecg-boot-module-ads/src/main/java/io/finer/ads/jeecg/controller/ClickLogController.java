package io.finer.ads.jeecg.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import io.finer.ads.jeecg.entity.ClickLog;
import io.finer.ads.jeecg.service.IClickLogService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 点击日志
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Api(tags="点击日志")
@RestController
@RequestMapping("/clickLog")
@Slf4j
public class ClickLogController extends JeecgController<ClickLog, IClickLogService> {
	@Autowired
	private IClickLogService clickLogService;

	/**
	 * 分页列表查询
	 *
	 * @param clickLog
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "点击日志-分页列表查询")
	@ApiOperation(value="点击日志-分页列表查询", notes="点击日志-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ClickLog clickLog,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ClickLog> queryWrapper = QueryGenerator.initQueryWrapper(clickLog, req.getParameterMap());
		Page<ClickLog> page = new Page<ClickLog>(pageNo, pageSize);
		IPage<ClickLog> pageList = clickLogService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param clickLog
	 * @return
	 */
	@AutoLog(value = "点击日志-添加")
	@ApiOperation(value="点击日志-添加", notes="点击日志-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ClickLog clickLog) {
		clickLogService.save(clickLog);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param clickLog
	 * @return
	 */
	@AutoLog(value = "点击日志-编辑")
	@ApiOperation(value="点击日志-编辑", notes="点击日志-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ClickLog clickLog) {
		clickLogService.updateById(clickLog);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "点击日志-通过id删除")
	@ApiOperation(value="点击日志-通过id删除", notes="点击日志-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		clickLogService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "点击日志-批量删除")
	@ApiOperation(value="点击日志-批量删除", notes="点击日志-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.clickLogService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "点击日志-通过id查询")
	@ApiOperation(value="点击日志-通过id查询", notes="点击日志-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ClickLog clickLog = clickLogService.getById(id);
		if(clickLog==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(clickLog);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param clickLog
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ClickLog clickLog) {
        return super.exportXls(request, clickLog, ClickLog.class, "点击日志");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ClickLog.class);
    }

}
